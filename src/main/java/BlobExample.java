import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;

public class BlobExample {

    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
        String url = "jdbc:mysql://localhost:3306/first_lesson?" +
                "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String login = "root";
        String pass = "";

        Class.forName("com.mysql.cj.jdbc.Driver");

        try (Connection connection = DriverManager.getConnection(url, login, pass)) {
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE Images (name VARCHAR (15), d DATE, image BLOB)");

            PreparedStatement prepStatement = null;
            try{
                BufferedImage image = ImageIO.read(new File("smile-min.png"));
                Blob smile = connection.createBlob();
                try(OutputStream outputStream = smile.setBinaryStream(1)){
                    ImageIO.write(image, "png", outputStream);
                }

                prepStatement = connection.prepareStatement("INSERT  into images (name, d, image) VALUES (?, {d ?}, ?)");
                prepStatement.setString(1, "smile");
                prepStatement.setDate(2, Date.valueOf("2018-04-07"));
                prepStatement.setBlob(3, smile);
                prepStatement.execute();

                ResultSet rs = null;
                try {
                    rs = prepStatement.executeQuery("SELECT * FROM Images");
                    while(rs.next()){
                        Blob newSmile = rs.getBlob("image");
                        BufferedImage output = ImageIO.read(newSmile.getBinaryStream());
                        File outputFile = new File("saved.png");
                        ImageIO.write(output, "png", outputFile);
                    }
                }catch (SQLException ex){
                    ex.printStackTrace();
                }finally {
                    if(rs != null) {
                        rs.close();
                    }else{
                        System.out.println("Error read DataBase");
                    }
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }finally {
                prepStatement.close();
            }
        }
    }
}
