import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Scanner;

public class TestConnection {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {

        //Задаем параметры подключения: url, имя пользователя, пароль
        String url = "jdbc:mysql://localhost:3306/first_lesson?" +
                "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String userName = "root";
        String pass = "";

        //Регистрируем драйвер с помощью статического инициализатора
        Class.forName("com.mysql.cj.jdbc.Driver");

        //Создаем подключение, передавая в getConnection
        try (Connection connection = DriverManager.getConnection(url, userName, pass);
             BufferedReader sqlFile = new BufferedReader(new FileReader("/Users/macbookpro/IdeaProjects/StudyPractice/" +
                     "JDBCPractice/src/main/java/books.sql"));
             Scanner scan = new Scanner(sqlFile);
             Statement statement = connection.createStatement()) {

            String line;

            while (scan.hasNextLine()) {
                line = scan.nextLine();
                if (line.endsWith(";")) {
                    line = line.substring(0, line.length() - 1);
                }
                statement.executeUpdate(line);
            }

            ResultSet rs = null;
            try {
                rs = statement.executeQuery("SELECT * FROM Books");
                while (rs.next()) {
                    int id = rs.getInt("1");
                    String name = rs.getString("2");
                    double price = rs.getDouble("3");

                    System.out.println("id: " + id + "name: " + name + "price: " + price);
                }
            } catch (SQLException e) {
                System.err.println("SQLException message: " + e.getMessage());
                System.err.println("SQLException SQL state: " + e.getSQLState());
                System.err.println("SQLException error code: " + e.getErrorCode());
            } finally {
                if (rs != null) {
                    rs.close();
                } else {
                    System.err.println("Ошибка чтения данных с БД");
                }
            }
        }
    }
}
