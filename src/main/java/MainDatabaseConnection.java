import java.sql.*;

public class MainDatabaseConnection {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        String url = "jdbc:mysql://localhost:3306/first_lesson?" +
                "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String login = "root";
        String pass = "";

        Class.forName("com.mysql.cj.jdbc.Driver");

        try (Connection connection = DriverManager.getConnection(url, login, pass)) {
            PreparedStatement prepStat = null;
            try {
                prepStat = connection.prepareStatement("INSERT into books (bookName , price) VALUES (?,?)");
                prepStat.setString(1, "Schindler's list");
                prepStat.setDouble(2, 32.5);
                prepStat.execute();

                ResultSet rs = null;
                try {
                    rs = prepStat.executeQuery("SELECT * from books");
                    while (rs.next()) {
                        int id = rs.getInt(1);
                        String name = rs.getString(2);
                        double price = rs.getDouble(3);
                        System.out.println("id " + id + ", name " + name + ", price " + price);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    if (rs != null) {
                        rs.close();
                    } else {
                        System.out.println("Ошибка чтения с БД");
                    }
                }
            } catch (SQLException es) {
                es.printStackTrace();
            } finally {
                prepStat.close();
            }

            CallableStatement callStat = null;
            try {
                callStat = connection.prepareCall("{CALL booksCount(?)}");
                callStat.registerOutParameter(1, Types.INTEGER);
                callStat.execute();
                System.out.println("Количество запией в таблице " + callStat.getInt(1));
            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                callStat.close();
            }

        }

    }
}
