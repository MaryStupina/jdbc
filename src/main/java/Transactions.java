import java.sql.*;

public class Transactions {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        String url = "jdbc:mysql://localhost:3306/first_lesson?" +
                "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String login = "root";
        String pass = "";

        Class.forName("com.mysql.cj.jdbc.Driver");

        try (Connection connection = DriverManager.getConnection(url, login, pass);
             Statement statement = connection.createStatement()) {

            String createTable = "CREATE TABLE Fruit (name VARCHAR(15) NOT NULL, amount int, price double NOT NULL, PRIMARY KEY(name))";
            String comand1 = "INSERT INTO Fruit (name, amount, price) VALUES ('Apple', 200, 3.50)";
            String comand2 = "INSERT INTO Fruit (name, amount, price) VALUES ('Orange', 50, 5.50)";
            String comand3 = "INSERT INTO Fruit (name, amount, price) VALUES ('Lemon', 300, 5.50)";
            String comand4 = "INSERT INTO Fruit (name, amount, price) VALUES ('Pineapple', 20, 7.50)";

//            connection.setAutoCommit(false);// отключить режим автоматической фиксации
//            statement.executeUpdate(createTable);
//            statement.executeUpdate(comand1);
//            Savepoint svp = connection.setSavepoint(); // создаем точку сохранения. Откат происходит до этой точки при его передаче в метод отката
//            statement.executeUpdate(comand2);
//            statement.executeUpdate(comand3);
//            statement.executeUpdate(comand4);
////            connection.commit();
//
//            connection.rollback(svp); // откатить изменения. Откатываются только изменения, создания таблицы не откатывается
//            connection.commit();
//            connection.releaseSavepoint(svp);

            // механизм групповых обновлений вместо выполнения по отдельности однотипных команд(например вставкив таблицу)
            connection.setAutoCommit(true);
            statement.addBatch(createTable);
            statement.addBatch(comand1);
            statement.addBatch(comand2);
            statement.addBatch(comand3);
            statement.addBatch(comand4);
            statement.executeBatch();
        }
    }
}
