
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.sql.*;

public class RowSet {

    static String url = "jdbc:mysql://localhost:3306/first_lesson?" +
            "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static String login = "root";
    static String pass = "";


    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        ResultSet resultSet = getResSet();
        while (resultSet.next()){
            System.out.println(resultSet.getString("bookName"));
        }

        CachedRowSet rowSet = (CachedRowSet) resultSet;
        rowSet.setCommand("SELECT * FROM books WHERE price > ?");
        rowSet.setDouble(1, 30.0);

        //необходимо создать подключение к БД
        rowSet.setUrl(url);
        rowSet.setUsername(login);
        rowSet.setPassword(pass);
        rowSet.execute();

        //произвести изменения в БД используя rowSet
        rowSet.absolute(2);//перейти на указанную строку
        rowSet.deleteRow();// удалить строку
        rowSet.beforeFirst();//перейти на строку ранее

        //чтобы изменения вступили в силу необходимо создать соединение
        Connection connection = DriverManager.getConnection(url, login, pass);
        connection.setAutoCommit(false);// выставить чтобы изменения внеслись корректно
        rowSet.acceptChanges(connection);


        while(rowSet.next()){
            String name = rowSet.getString("bookName");
            double price = rowSet.getDouble("price");
            System.out.println(name + ", " + price);
        }

    }

    static ResultSet getResSet() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");

        try (Connection conn = DriverManager.getConnection(url, login, pass);
             Statement stat = conn.createStatement()) {

            ResultSet rs = stat.executeQuery("SELECT * FROM books");
            RowSetFactory factory = RowSetProvider.newFactory();
            CachedRowSet crs = factory.createCachedRowSet();
            crs.populate(rs);
            return crs;
        }
    }
}
