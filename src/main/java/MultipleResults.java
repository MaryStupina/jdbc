import java.sql.*;

public class MultipleResults {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        String url = "jdbc:mysql://localhost:3306/first_lesson?" +
                "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String login = "root";
        String pass = "";

        Class.forName("com.mysql.cj.jdbc.Driver");

        try (Connection connection = DriverManager.getConnection(url, login, pass)) {
            CallableStatement callableStatement = null;
            try{
                callableStatement = connection.prepareCall("{CALL tablesCount}");
                boolean hasResults = callableStatement.execute();
                ResultSet resultSet = null;

                try{
                    while (hasResults){
                        resultSet = callableStatement.getResultSet();
                        while (resultSet.next()){
                            System.out.println("Row number in a table is: " + resultSet.getInt(1));
                        }

                        hasResults = callableStatement.getMoreResults();
                    }
                }catch (SQLException ex){
                    ex.printStackTrace();
                }finally {
                    if(resultSet != null) {
                        resultSet.close();
                    }else{
                        System.out.println("Error read DataBase");
                    }
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }finally {
                callableStatement.close();
            }
        }
    }
}
