import java.sql.*;

public class TypesOfReading {
    static String url = "jdbc:mysql://localhost:3306/first_lesson?" +
            "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static String login = "root";
    static String pass = "";

    public static void main(String[] args) throws SQLException, InterruptedException {
        try (Connection connection = DriverManager.getConnection(url, login, pass);
             Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);

            //1. пример "грязного чтения"
//            connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED); // изменение уровня изоляции БД
//
//            statement.executeUpdate("UPDATE Books SET price = 100 WHERE bookId = 1");
//
//            new OtherTransaction().run();
//            Thread.sleep(2000);
//            connection.rollback();

            //2. пример "неповторяющегося чтения"
//            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
//            ResultSet resultSet = statement.executeQuery("SELECT * FROM Books");
//            while (resultSet.next()){
//                System.out.println(resultSet.getString("bookName") + ", " + resultSet.getDouble("price"));
//            }
//            new OtherTransaction().run();
//            Thread.sleep(2000);
//            ResultSet resultSet2 = statement.executeQuery("SELECT * FROM Books");
//            while (resultSet2.next()){
//                System.out.println(resultSet2.getString("bookName") + ", " + resultSet2.getDouble("price"));
//            }
            //3. пример "фантомного чтения"
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Books WHERE bookId > 5");
            while (resultSet.next()){
                System.out.println(resultSet.getString("bookName") + ", " + resultSet.getDouble("price"));
            }
            new OtherTransaction().run();
            Thread.sleep(2000);

            ResultSet resultSet2 = statement.executeQuery("SELECT * FROM Books WHERE bookId > 5");
            while (resultSet2.next()){
                System.out.println(resultSet2.getString("bookName") + ", " + resultSet2.getDouble("price"));
            }

        }
    }

    static class OtherTransaction extends Thread{

        @Override
        public void run(){
            try (Connection connection = DriverManager.getConnection(url, login, pass);
                 Statement statement = connection.createStatement()) {
                connection.setAutoCommit(false);

                //1. пример "грязного чтения"
//                connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
//                ResultSet rs = statement.executeQuery("SELECT * FROM Books");
//                while (rs.next()){
//                    System.out.println(rs.getString("bookName") + ", " + rs.getDouble("price"));
//                }

                //2. пример "неповторяющегося чтения"
//                statement.executeUpdate("UPDATE Books SET price = price+20 WHERE bookName = 'Solomon key'");
//                connection.commit();

                //3. пример "фантомного чтения"
                connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                statement.executeUpdate("INSERT INTO Books (bookName, price) VALUES ('newBook', 10.0)");

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
