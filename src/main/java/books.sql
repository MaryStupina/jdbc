create table Books (bookId int NOT NULL AUTO_INCREMENT,
                    bookName varchar(30) NOT NULL,
                    price double,
                    PRIMARY KEY (bookId));

INSERT into Books (bookName, price) values ('Inferno', 45.0);
INSERT into Books (bookName, price) values ('Harry Poter', 45.5);
INSERT into Books (bookName, price) values ('It', 25.0);
INSERT into Books (bookName, price) values ('Spartacus', 55.5);
INSERT into Books (bookName, price) values ('Green Mile', 20.6);
INSERT into Books (bookName, price) values ('Solomon Key', 5.0);