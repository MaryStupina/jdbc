import java.sql.*;

public class ScrollableResultSet {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        String url = "jdbc:mysql://localhost:3306/first_lesson?" +
                "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String login = "root";
        String pass = "";

        Class.forName("com.mysql.cj.jdbc.Driver");

        try (Connection connection = DriverManager.getConnection(url, login, pass);
             Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)){
                ResultSet rs = null;
                try{
                    rs = statement.executeQuery("SELECT * FROM books");
//                    while(rs.next()){
//                        int id = rs.getInt(1);
//                        double price = rs.getDouble(3);
//
//                        if(id == 4){
//                            rs.updateString(2, "Spartacus (discount)");
//                            rs.updateDouble(3, price-10);
//                            rs.updateRow();
//                        }
//
//                    }
                    if(rs.absolute(2)) System.out.println(rs.getString("bookName"));//переместиться на 2 строку
                    if(rs.previous()) System.out.println(rs.getString("bookName"));//переместиться на предыдущую строку
                    if(rs.last()) System.out.println(rs.getString("bookName"));//переместиться на последнюю строку
                    if(rs.relative(-3)) System.out.println(rs.getString("bookName"));
                    {//переместиться на 3 строки назад

                        ResultSetMetaData rsmd = rs.getMetaData();
                        while (rs.next()){
                            for(int i=1; i<=rsmd.getColumnCount(); ++i){
                                String field = rsmd.getColumnName(i);
                                String value = rs.getString(field);
                                System.out.print(field + " " + value + ", ");
                            }
                            System.out.println("");
                        }
                    }

                }catch (SQLException ex){
                    ex.printStackTrace();
                }finally {
                    if(rs != null){
                        rs.close();
                    }else{
                        System.out.println("Ошибка чтения с БД");
                    }
                }
        }
    }
}
